#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <glib.h>
#include <gtk/gtk.h>
#include "drawgon.h"

static GtkEntry *COMMAND;
GtkWidget *darea;
GtkWidget *help_dialog;

void on_main_window_destroy(void)
{
    gtk_main_quit();
}

void on_btn_line_clicked(void)
{
    gtk_entry_set_text(COMMAND, "line:");
}

void on_btn_circle_clicked(void)
{
    gtk_entry_set_text(COMMAND, "circle:");
}

void on_btn_rectangle_clicked(void)
{
    gtk_entry_set_text(COMMAND, "rectangle:");
}

void on_btn_triangle_clicked(void)
{
    gtk_entry_set_text(COMMAND, "triangle:");
}

void on_btn_clear_clicked(void)
{
    gtk_entry_set_text(COMMAND, "clear:");
}

void on_btn_clear_selection_clicked(void)
{
    gtk_entry_set_text(COMMAND, "sclear:");
}

void on_btn_translate_clicked(void)
{
    gtk_entry_set_text(COMMAND, "translate:");
}

void on_btn_rotate_clicked(void)
{
    gtk_entry_set_text(COMMAND, "rotate:");
}

void on_btn_scale_clicked(void)
{
    gtk_entry_set_text(COMMAND, "scale:");
}

void on_btn_select_clicked(void)
{
    gtk_entry_set_text(COMMAND, "select:");
}

void on_btn_exec_clicked(void)
{
    drawgon_exec(gtk_entry_get_text(COMMAND));
}

void on_btn_help_clicked(void)
{
    gtk_widget_show(help_dialog);
}

void on_btn_help_ok_clicked(void)
{
    gtk_widget_hide(help_dialog);
}

static void on_draw_event(GtkWidget *widget, cairo_t *cr)
{
    drawgon_draw_all(widget, cr);
}

static void on_motion_notify_event(GtkWidget *w, GdkEventMotion *ev, gpointer g)
{
    GtkLabel *lblpoint = g;
    char point[255];
    int dx, dy;
    double x, y;

    gtk_widget_translate_coordinates(w, darea, 0, 0, &dx, &dy);
    x = ev->x + dx;
    y = ev->y + dy;
    sprintf(point, "(%.0lf,%.0lf)", x, y);
    gtk_label_set_text(lblpoint, point);
}

static void clicked(GtkWidget *widget, GdkEventButton *ev)
{
    int dx, dy;
    double x, y;
    char click[255];
    static int clicks = 0;
    
    gtk_widget_translate_coordinates(widget, darea, 0, 0, &dx, &dy);
    x = ev->x + dx;
    y = ev->y + dy;

    /* Ta emitindo 2 sinais pra cada clique. */
    if (++clicks % 2 && x >= 0 && y >= 0) {
	sprintf(click, "%s(%.0lf,%.0lf)",
		gtk_entry_get_text(COMMAND), x, y);
	gtk_entry_set_text(COMMAND, click);
    } else clicks = 0;
}

int main(int argc, char **argv)
{
    GtkBuilder *builder;
    GtkWidget *window;
    GtkWidget *frame;
    
    gtk_init(&argc, &argv);
    builder = gtk_builder_new_from_file(argv[1]);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "main_window"));
    help_dialog = GTK_WIDGET(gtk_builder_get_object(builder, "help_dialog"));
    frame = GTK_WIDGET(gtk_builder_get_object(builder, "frame"));;
    COMMAND = GTK_ENTRY(gtk_builder_get_object(builder, "command"));
    darea = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(frame), darea);
    g_signal_connect(darea, "draw", G_CALLBACK(on_draw_event), NULL);
    g_signal_connect(window, "button-press-event", G_CALLBACK(clicked), NULL);
    g_signal_connect(window, "motion-notify-event",
		     G_CALLBACK(on_motion_notify_event),
		     gtk_builder_get_object(builder, "lbl_point"));
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(builder);
    gtk_widget_show_all(window);
    gtk_main();

    return 0;
}
