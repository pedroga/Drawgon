#pragma once

#include <gtk/gtk.h>
#include <cairo.h>
#include <stdbool.h>

bool drawgon_exec(const char *cmd);
void drawgon_draw_all(GtkWidget *widget, cairo_t *cr);
